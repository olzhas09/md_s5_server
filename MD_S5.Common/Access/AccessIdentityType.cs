﻿namespace MD_S5.Common.Access
{
    public enum AccessIdentityType : byte
    {
        User = 0,
        Group = 1,
    }
}