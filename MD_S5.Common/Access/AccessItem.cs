﻿using System;

namespace MD_S5.Common.Access
{
    [Serializable]
    public class AccessItem
    {
        public AccessIdentityType AccessIdentity { get; set; }
        public long IdentityId { get; set; }
        public AccessType AccessType { get; set; }
    }
}