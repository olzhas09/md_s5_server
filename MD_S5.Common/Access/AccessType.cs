﻿using System;

namespace MD_S5.Common.Access
{
    [Flags]
    public enum AccessType
    {
        None = 0,
        Create = 1,
        Read = 2,
        Update = 4,
        Delete = 8,
    }
}