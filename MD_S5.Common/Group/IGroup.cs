﻿namespace MD_S5.Common.Group
{
    public interface IGroup<T>
    {
        public T ParentId { get; set; }
        public IGroup<T> Parent { get; set; }
    }
}