﻿using MD_S5.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MD_S5.Common.Group
{
    public enum WbsType
    {
        Parent = 0,
        Group = 1,
        Childs = 2
    }

    public class Wbs : Entity<long>
    {
        public long? ParentId { get; set; }
        public virtual Wbs Parent { get; set; }
        public long? TypeId { get; set; }
        public string Type { get; set; }
        public long? ReferenceId { get; set; }
        public string ReferenceType { get; set; }

        [NotMapped]
        public object MainRef { get; set; }

        [NotMapped]
        public object Reference { get; set; }

        [NotMapped]
        public List<object> ListType { get; set; }

        public WbsType WbsType { get; set; }
    }

    public static class TreeExtensions
    {
        /// <summary> Generic interface for tree node structure </summary>
        /// <typeparam name="T"></typeparam>
        public interface ITree<T>
        {
            T Data { get; }
            ITree<T> Parent { get; }
            ICollection<ITree<T>> Children { get; }
            bool IsRoot { get; }
            bool IsLeaf { get; }
            int Level { get; }
        }

        /// <summary> Flatten tree to plain list of nodes </summary>
        public static IEnumerable<TNode> Flatten<TNode>(this IEnumerable<TNode> nodes, Func<TNode, IEnumerable<TNode>> childrenSelector)
        {
            if (nodes == null) throw new ArgumentNullException(nameof(nodes));
            return nodes.SelectMany(c => childrenSelector(c).Flatten(childrenSelector)).Concat(nodes);
        }

        /// <summary> Converts given list to tree. </summary>
        /// <typeparam name="T">Custom data type to associate with tree node.</typeparam>
        /// <param name="items">The collection items.</param>
        /// <param name="parentSelector">Expression to select parent.</param>
        public static ITree<T> ToTree<T>(this IList<T> items, Func<T, T, bool> parentSelector)
        {
            if (items == null) throw new ArgumentNullException(nameof(items));
            var lookup = items.ToLookup(item => items.FirstOrDefault(parent => parentSelector(parent, item)),
                child => child);
            return Tree<T>.FromLookup(lookup);
        }

        /// <summary> Internal implementation of <see cref="ITree{T}" /></summary>
        /// <typeparam name="T">Custom data type to associate with tree node.</typeparam>
        internal class Tree<T> : ITree<T>
        {
            public T Data { get; }
            public ITree<T> Parent { get; private set; }
            public ICollection<ITree<T>> Children { get; }
            public bool IsRoot => Parent == null;
            public bool IsLeaf => Children.Count == 0;
            public int Level => IsRoot ? 0 : Parent.Level + 1;

            private Tree(T data)
            {
                Children = new LinkedList<ITree<T>>();
                Data = data;
            }

            public static Tree<T> FromLookup(ILookup<T, T> lookup)
            {
                var rootData = lookup.Count == 1 ? lookup.First().Key : default;
                var root = new Tree<T>(rootData);
                root.LoadChildren(lookup);
                return root;
            }

            private void LoadChildren(ILookup<T, T> lookup)
            {
                foreach (var data in lookup[Data])
                {
                    var child = new Tree<T>(data) { Parent = this };
                    Children.Add(child);
                    child.LoadChildren(lookup);
                }
            }
        }
    }

    public class WbsByField
    {
        public Guid Guid { get; set; }
        public long? ParentId { get; set; }
        public virtual WbsByField Parent { get; set; }
        public long? TypeId { get; set; }
        public string TypeName { get; set; }

        [NotMapped]
        public object Type { get; set; }

        public long? ReferenceId { get; set; }
        public string ReferenceName { get; set; }

        [NotMapped]
        public object Reference { get; set; }

        [NotMapped]
        public List<object> Childs { get; set; }

        public WbsType WbsType { get; set; }
    }
}