﻿using MD_S5.Common.Access;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace MD_S5.Common.Model
{
    public class PaginatorContainer<T>
    {
        public int CountTotal { get; set; }
        public List<T> Items { get; set; }
    }

    public class Temp : Entity<long>
    {
        public string TypeName { get; set; }
        public string ValueId { get; set; }
        public string Value { get; set; }

        [NotMapped]
        public List<string> ChangeProperty { get; set; }

        public string SavedStatus { get; set; }

        [NotMapped]
        public IEntity<long> Obj { get; set; }

        [NotMapped]
        public long? RowIndex { get; set; }

        [NotMapped]
        public object User { get; set; }
    }

    public delegate bool TryParseHandler<T>(string value, out T result);

    public abstract class Entity<TKey> : NotificationEntity, INotifyPropertyChanged, IEntity<TKey> where TKey : struct
    {
        public static T? TryParse<T>(string value, TryParseHandler<T> handler) where T : struct
        {
            if (string.IsNullOrEmpty(value))
                return null;
            T result;
            if (handler(value, out result))
                return result;
            Trace.TraceWarning("Invalid value '{0}'", value);
            return null;
        }

        private List<AccessItem> accesses;
        private TKey id;
        private DateTime createDate;
        private DateTime updateDate;
        private DateTime stamp;
        private StatusModel status;
        private long? ownerId;
        private Project owner;
        private long? createUserId;
        private long? updateUserId;

        [Key]
        public TKey Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime Stamp { get; set; }
        public StatusModel Status { get; set; }
        public long? OwnerId { get; set; }
        public virtual Project Owner { get; set; }
        public byte[] PermissionByte { get; set; }
        public long? CreateUserId { get; set; }
        public long? UpdateUserId { get; set; }

        [NotMapped]
        public bool? Editing { get; set; }

        public static bool FindByAllColumns(IEntity<TKey> obj, string value, int depth = 0)
        {
            depth++;
            var result = false;
            var properties = obj?.GetType()?.GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                var propety = properties[i];
                var propertyValue = propety.GetValue(obj, null);
                if (propety.PropertyType.Namespace.Contains("MD_S5") && propertyValue is IEntity<TKey> && !propety.DeclaringType.IsEnum && propertyValue != null && depth < 2)
                {
                    result = FindByAllColumns((IEntity<TKey>)propertyValue, value, depth);
                }
                else
                if (propertyValue != null && propertyValue.ToString().ToLower().Contains(value.ToLower()))
                {
                    result = true;
                }
                if (result)
                {
                    return result;
                }
            }
            return result;
        }

        //[NotMapped]
        //public List<AccessItem> Accesses
        //{
        //    get => FromByteArray<List<AccessItem>>(PermissionByte);
        //    set
        //    {
        //        PermissionByte = ToByteArray(value);
        //        accesses = value;
        //    }
        //}

        public string Name { get; set; }
        public string NameAlternative1 { get; set; }
        public string NameAlternative2 { get; set; }

        [NotMapped]
        public ImportStatus ImportStatus { get; set; }

        //public byte[] ToByteArray<T>(T obj)
        //{
        //    if (obj == null)
        //        return null;
        //    var bf = new BinaryFormatter();
        //    using (var ms = new MemoryStream())
        //    {
        //        bf.Serialize(ms, obj);
        //        return ms.ToArray();
        //    }
        //}

        //public T FromByteArray<T>(byte[] data)
        //{
        //    if (data == null)
        //        return default;
        //    var bf = new BinaryFormatter();
        //    using (var ms = new MemoryStream(data))
        //    {
        //        object obj = bf.Deserialize(ms);
        //        return (T)obj;
        //    }
        //}
    }

    public class NotificationEntity : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void SetWithNotify<T>(T value, ref T field, [CallerMemberName] string propertyName = "")
        {
            if (!Equals(field, value))
            {
                field = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}