﻿using System;

namespace MD_S5.Common.Model
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
        string Name { get; set; }
        string NameAlternative1 { get; set; }
        string NameAlternative2 { get; set; }
        DateTime CreateDate { get; set; }
        DateTime UpdateDate { get; set; }
        DateTime Stamp { get; set; }
        StatusModel Status { get; set; }

        //public byte[] PermissionByte { get; set; }
        long? CreateUserId { get; set; }

        long? UpdateUserId { get; set; }
        long? OwnerId { get; set; }
        ImportStatus ImportStatus { get; set; }
    }

    public enum ImportStatus
    {
        Create = 0,
        Find = 1,
        NotFound = 2
    }
}