﻿using System;

namespace MD_S5.Common.Notify
{
    public class NotifyEventArgs : EventArgs
    {
        public NotifyEventArgs(string userId, string type, string operation, string message)
        {
            UserId = userId;
            Message = message;
            Operation = operation;
            Type = type;
        }

        public string UserId { get; set; }
        public string Type { get; set; }
        public string Operation { get; set; }
        public string Message { get; set; }
    }
}