﻿using System;

namespace MD_S5.Common.Notify
{
    public static class NotifyService
    {
        public static event EventHandler<NotifyEventArgs> OnNotify;

        public static void NotifyAll(string userId, string type, string operation, string message)
        {
            EventHandler<NotifyEventArgs> handler = OnNotify;
            handler?.Invoke(null, new NotifyEventArgs(userId, type, operation, message));
        }
    }
}