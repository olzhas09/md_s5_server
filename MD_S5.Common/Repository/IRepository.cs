﻿using MD_S5.Common.Group;
using MD_S5.Common.Model;
using MD_S5.Excell;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MD_S5.Common.Repository
{
    public interface IRepository<TEntity, TKey>
    {
        string RepositoryName { get; }

        Task<IEnumerable<TEntity>> Get();

        IEnumerable<TEntity> Where(Func<TEntity, bool> func);

        Task<TEntity> GetById(TKey id);

        Task<TEntity> Create(TEntity entity);

        Task<TEntity> Update(TEntity entity);

        Task<TEntity> Delete(TKey id);

        Task<TEntity> Delete(TEntity entity);

        Task<TEntity> Save(TEntity entity);

        Task<PaginatorContainer<TEntity>> GetOrFind(string order, int skip, int take, Column[] colums, string value = "", string property = "", int desc = 1);

        Task<IEnumerable<Temp>> History(TKey id);

        Task<IEnumerable<WbsByField>> GetWbsByFields(long? id);

        Task<FileStreamResult> ExportExcel(Column[] cols);

        Task<UploadFile> ImportExcelStepOne(Stream fileStream);

        Task<MatchColumn> ImportExcelStepTwo(Stream fileStream, List<Column> columns, List<Column> columnsExport);

        Task<MatchColumn> ImportExcelStepThree(string guid);

        Task<IEnumerable<TEntity>> GetUsingRedisCache();

        Task<IEnumerable<object>> CreateRange(IEnumerable<object> range);

        Task<IEnumerable<object>> UpdateRange(IEnumerable<object> range);
    }
}