﻿using System.Collections.Generic;

namespace MD_S5.Excell
{
    public class Column
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string Field { get; set; }
        public string SubField { get; set; }
        public Column SubColumn { get; set; }
        public bool CreateReference { get; set; }
        public bool UniqueField { get; set; }
        public bool CreateRef { get; set; }
        public int Issue { get; set; }
        public int Active { get; set; }
        public int MetaData { get; set; }
        public string Example1 { get; set; }
        public string Example2 { get; set; }
        public string Type { get; set; }
    }

    public class UploadFile
    {
        public string NameFile { get; set; }
        public long SizeFile { get; set; }
        public string TypeFile { get; set; }
        public int RowsCount { get; set; }
        public int ColumnsCount { get; set; }
        public List<Column> Columns { get; set; }
        public string Preview { get; set; }
    }

    public class MatchColumn
    {
        public string Guid { get; set; }
        public int UpdateRows { get; set; }
        public int CreateRows { get; set; }
        public List<Column> Columns { get; set; }
        public List<object> Entities { get; set; }
    }
}