﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MD_S5.Excell
{
    public class ExcellFile
    {
        public MemoryStream Stream { get; set; }
        public string Name { get; set; }
    }

    public static class StringExtensions
    {
        public static string FirstCharToUpper(this string input) =>
            input switch
            {
                null => throw new ArgumentNullException(nameof(input)),
                "" => throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input)),
                _ => input.First().ToString().ToUpper() + input.Substring(1)
            };
    }

    public class ExportExcel
    {
        public static async Task<(MemoryStream, string)> Create(IList list, string path, Column[] cols)
        {
            var arr = cols.Select(x => new Column { Field = x.Field?.FirstCharToUpper(), Header = x.Header, SubField = x.SubField?.FirstCharToUpper() }).ToArray();
            return await CreateProperty(list, path, arr);
        }

        public static async Task<(MemoryStream, string)> CreateProperty(IList list, string path, Column[] cols)
        {
            var itemsDic = new Dictionary<string, Dictionary<string, object>>();
            foreach (var item in list)
            {
                var propertyId = item.GetType().GetProperty("Id");
                var id = propertyId.GetValue(item);
                var itemDic = new Dictionary<string, object>();
                foreach (var propertyName in cols)
                {
                    if (propertyName.Field == null)
                    {
                        continue;
                    }
                    var property = item.GetType().GetProperty(propertyName.Field);
                    if (property == null)
                    {
                        continue;
                    }
                    var val = property.GetValue(item);
                    if (!string.IsNullOrEmpty(propertyName.SubField) && val != null)
                    {
                        var subObject = val.GetType();
                        var subField = subObject.GetProperty(propertyName.SubField);
                        var subFieldValue = subField.GetValue(val);
                        itemDic.TryAdd(propertyName.Header, subFieldValue ?? " ");
                    }
                    else
                    {
                        itemDic.TryAdd(propertyName.Header, val ?? " ");
                    }
                }
                itemsDic.Add(id.ToString(), itemDic);
            }
            return await CreateExcel(itemsDic, path, cols);
        }

        public static async Task<(MemoryStream, string)> CreateExcel(Dictionary<string, Dictionary<string, object>> dic, string path, Column[] cols)
        {
            var memoryStream = new MemoryStream();
            {
                using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
                {
                    var workbookpart = document.AddWorkbookPart();
                    workbookpart.Workbook = new Workbook();

                    // Add a WorksheetPart to the WorkbookPart.
                    var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                    var sheetData = new SheetData();
                    worksheetPart.Worksheet = new Worksheet(sheetData);

                    var sheet = new Sheet()
                    {
                        Id = document.WorkbookPart.
                     GetIdOfPart(worksheetPart),
                        SheetId = 1,
                        Name = "mySheet"
                    };

                    var sheets = document.WorkbookPart.Workbook.
                        AppendChild<Sheets>(new Sheets());
                    sheets.Append(sheet);

                    // Append a new worksheet and associate it with the workbook.
                    char[] alphabet = Enumerable.Range('A', 50).Select(x => (char)x).ToArray();
                    var i = 0;
                    var row = new Row { RowIndex = 1 };
                    foreach (var item in dic)
                    {
                        foreach (var col in item.Value)
                        {
                            try
                            {
                                var value = col.Key;
                                var header = new Cell() { CellReference = $"{alphabet[i]}1", CellValue = new CellValue(value.ToString()), DataType = CellValues.String, };
                                row.Append(header);
                                i++;
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        sheetData.Append(row);
                        break;
                    }
                    int j = 2;
                    foreach (var item in dic)
                    {
                        i = 0;
                        row = new Row { RowIndex = UInt32Value.FromUInt32((uint)j) };
                        foreach (var col in item.Value)
                        {
                            if (col.Value == null)
                                continue;
                            var value = col.Value.ToString();
                            var header = new Cell() { CellReference = $"{alphabet[i]}{j}", CellValue = new CellValue(value), DataType = CellValues.String };
                            row.Append(header);
                            i++;
                        }
                        sheetData.Append(row);
                        j++;
                    }
                    workbookpart.Workbook.Save();
                    document.Close();
                }
                memoryStream.Seek(0, SeekOrigin.Begin);
                var excellFile = new ExcellFile() { Stream = memoryStream, Name = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" };
                return await Task.FromResult((memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
            }
        }

        public static object GetPropertyValue(object source, string propertyName)
        {
            var property = source.GetType().GetProperty(propertyName);
            return property.GetValue(source, null);
        }
    }
}