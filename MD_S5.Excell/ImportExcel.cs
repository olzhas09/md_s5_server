﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MD_S5.Excell
{
    public class ImportExcel
    {
        private static Dictionary<int, Dictionary<string, string>> items = new Dictionary<int, Dictionary<string, string>>();
        private static SpreadsheetDocument document;

        public static async Task<UploadFile> GetHeader(Stream stream)
        {
            var d = ReadExcelSheet(stream, true);
            await Task.CompletedTask;
            return new UploadFile { Columns = d.Item2, RowsCount = d.Item1.Rows.Count };
        }

        public static (Dictionary<int, string> header, Dictionary<int, List<string>> value) ReadExcelAttributePage(Stream stream, bool firstRowIsHeader)
        {
            var headers = new List<string>();
            var dt = new DataTable();
            var headerDic = new Dictionary<int, string>();
            var valueDic = new Dictionary<int, List<string>>();
            using (var doc = SpreadsheetDocument.Open(stream, false))
            {
                Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
                IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
                int counter = 0;
                foreach (Row row in rows)
                {
                    counter = counter + 1;
                    if (counter == 1)
                    {
                        var j = 1;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            var colIndex = ColumnIndex(cell.CellReference);
                            var colunmName = firstRowIsHeader ? GetCellValue(doc, cell) : "Field" + j++;
                            Console.WriteLine(colunmName);
                            headers.Add(colunmName);
                            dt.Columns.Add(colunmName);
                            headerDic.Add(colIndex, colunmName);
                        }
                    }
                    else
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            //dt.Rows[dt.Rows.Count - 1][i] = GetCellValue(doc, cell);
                            var colIndex = ColumnIndex(cell.CellReference);
                            valueDic.TryGetValue(colIndex, out var list);
                            if (list == null)
                            {
                                list = new List<string>();
                                valueDic.TryAdd(colIndex, list);
                            }
                            list.Add(GetCellValue(doc, cell));
                            i++;
                        }
                    }
                }
            }
            return (headerDic, valueDic);
        }

        public static (Dictionary<int, string> header, Dictionary<int, List<string>> value) ReadExcelPage(Stream stream, bool firstRowIsHeader, string name)
        {
            var headers = new List<string>();
            var dt = new DataTable();
            var headerDic = new Dictionary<int, string>();
            var valueDic = new Dictionary<int, List<string>>();
            using (var doc = SpreadsheetDocument.Open(stream, false))
            {
                var sheet = doc.WorkbookPart.Workbook.Sheets.Cast<Sheet>().ToList().FirstOrDefault(x => x.Name.Value.Contains(name));
                //Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
                IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
                int counter = 0;
                foreach (Row row in rows)
                {
                    counter = counter + 1;
                    if (counter == 1)
                    {
                        var j = 1;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            var colIndex = ColumnIndex(cell.CellReference);
                            var colunmName = firstRowIsHeader ? GetCellValue(doc, cell) : "Field" + j++;
                            Console.WriteLine(colunmName);
                            headers.Add(colunmName);
                            dt.Columns.Add(colunmName);
                            headerDic.Add(colIndex, colunmName);
                        }
                    }
                    else
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            //dt.Rows[dt.Rows.Count - 1][i] = GetCellValue(doc, cell);
                            var colIndex = ColumnIndex(cell.CellReference);
                            valueDic.TryGetValue(colIndex, out var list);
                            if (list == null)
                            {
                                list = new List<string>();
                                valueDic.TryAdd(colIndex, list);
                            }
                            list.Add(GetCellValue(doc, cell));
                            i++;
                        }
                    }
                }
            }
            return (headerDic, valueDic);
        }

        public static List<Dictionary<string, string>> ReadRowExcelPage(Stream stream, bool firstRowIsHeader, string name)
        {
            var values = new List<Dictionary<string, string>>();
            var dt = new DataTable();
            using (var doc = SpreadsheetDocument.Open(stream, false))
            {
                var sheet = doc.WorkbookPart.Workbook.Sheets.Cast<Sheet>().ToList().FirstOrDefault(x => x.Name.Value.Contains(name));
                //Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
                IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
                var cols = new Dictionary<string, string>();
                foreach (Cell cell in rows.First().Descendants<Cell>())
                {
                    var colunmName = GetCellValue(doc, cell);
                    cols.Add(cell.CellReference.ToString().ToCharArray()[0].ToString(), colunmName);
                }
                var j = 2;
                foreach (Row row in rows)
                {
                    if (!firstRowIsHeader)
                    {
                        firstRowIsHeader = true;
                        continue;
                    }
                    var value = new Dictionary<string, string>();
                    foreach (var col in cols)
                    {
                        var cell = row.Descendants<Cell>().FirstOrDefault(x => x.CellReference == $"{col.Key}{j}");
                        string colunmName = "";
                        if (cell != null)
                            colunmName = GetCellValue(doc, cell);
                        if (string.IsNullOrEmpty(colunmName))
                            continue;
                        value.Add(col.Value, colunmName);
                    }
                    if (value.Keys.Count > 0 && value.Values.Count > 0)
                        values.Add(value);
                    j++;
                    //foreach (Cell cell in row.Descendants<Cell>())
                    //{
                    //    var colIndex = ColumnIndex(cell.CellReference);
                    //    value.Add("Id",colunmName);
                    //    values.Add(value);
                    //}
                    //counter = counter + 1;
                    //if (counter == 1)
                    //{
                    //    var j = 1;
                    //    foreach (Cell cell in row.Descendants<Cell>())
                    //    {
                    //        var colIndex = ColumnIndex(cell.CellReference);
                    //        var colunmName = firstRowIsHeader ? GetCellValue(doc, cell) : "Field" + j++;
                    //        Console.WriteLine(colunmName);
                    //        headers.Add(colunmName);
                    //        dt.Columns.Add(colunmName);
                    //        headerDic.Add(colIndex, colunmName);
                    //    }
                    //}
                    //else
                    //{
                    //    dt.Rows.Add();
                    //    int i = 0;
                    //    foreach (Cell cell in row.Descendants<Cell>())
                    //    {
                    //        //dt.Rows[dt.Rows.Count - 1][i] = GetCellValue(doc, cell);
                    //        var colIndex = ColumnIndex(cell.CellReference);
                    //        valueDic.TryGetValue(colIndex, out var list);
                    //        if (list == null)
                    //        {
                    //            list = new List<string>();
                    //            valueDic.TryAdd(colIndex, list);
                    //        }
                    //        list.Add(GetCellValue(doc, cell));
                    //        i++;
                    //    }
                    //}
                }
            }
            return values;
        }

        public static (DataTable, List<Column>) ReadExcelSheet(Stream stream, bool firstRowIsHeader)
        {
            var headers = new List<string>();
            var dt = new DataTable();
            using (var doc = SpreadsheetDocument.Open(stream, false))
            {
                Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
                IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
                int counter = 0;
                var headerDic = new Dictionary<int, string>();
                var valueDic = new Dictionary<int, List<string>>();
                foreach (Row row in rows)
                {
                    counter = counter + 1;
                    if (counter == 1)
                    {
                        var j = 1;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            var colIndex = ColumnIndex(cell.CellReference);
                            var colunmName = firstRowIsHeader ? GetCellValue(doc, cell) : "Field" + j++;
                            Console.WriteLine(colunmName);
                            headers.Add(colunmName);
                            dt.Columns.Add(colunmName);
                            headerDic.Add(colIndex, colunmName);
                        }
                    }
                    else
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            //dt.Rows[dt.Rows.Count - 1][i] = GetCellValue(doc, cell);
                            var colIndex = ColumnIndex(cell.CellReference);
                            valueDic.TryGetValue(colIndex, out var list);
                            if (list == null)
                            {
                                list = new List<string>();
                                valueDic.TryAdd(colIndex, list);
                            }
                            list.Add(GetCellValue(doc, cell));
                            i++;
                        }
                    }
                }
            }
            return (dt, headers.Select(x => new Column
            {
                Header = x,
                Example1 = dt.Rows[0][x].ToString(),
                Example2 = dt.Rows[1][x].ToString()
            }).ToList());
        }

        public static int ColumnIndex(string reference)
        {
            int ci = 0;
            reference = reference.ToUpper();
            for (int ix = 0; ix < reference.Length && reference[ix] >= 'A'; ix++)
                ci = (ci * 26) + ((int)reference[ix] - 64);
            return ci;
        }

        private static string GetCellValue(SpreadsheetDocument doc, Cell cell)
        {
            string value = cell.CellValue?.InnerText ?? "";
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
            }
            return value;
        }

        private static async Task<List<Column>> GetHeaders(SpreadsheetDocument document)
        {
            var mysheet = (Sheet)document.WorkbookPart.Workbook.Sheets.ChildElements.GetItem(0);
            var wbPart = document.WorkbookPart;
            var worksheet = ((WorksheetPart)wbPart.GetPartById(mysheet.Id)).Worksheet;
            var sheetDatas = worksheet.Elements<SheetData>();
            int i = 0;
            var columns = new List<Column>();
            foreach (var sheetData in sheetDatas)
            {
                var rows = sheetData.Elements<Row>();
                foreach (var row in rows)
                {
                    var dic = new Dictionary<string, string>();
                    foreach (var cell in row.Elements<Cell>())
                    {
                        if (cell.DataType == null)
                        {
                            if (cell.InnerText != null)
                            {
                                var columnValue = GetColumn(cell);
                                var rowIndex = GetRowIndex(cell);
                                var cellValue = cell.InnerText;
                                if (int.Parse(rowIndex) >= 4)
                                {
                                    dic.Add(columnValue, cellValue);
                                }
                            }
                        }
                        else
                        if (cell.DataType == CellValues.SharedString)
                        {
                            var columnValue = GetColumn(cell);
                            var rowIndex = GetRowIndex(cell);
                            var cellValue = await GetValue(int.Parse(cell.CellValue.InnerText));
                            if (int.Parse(rowIndex) >= 4)
                            {
                                dic.Add(columnValue, cellValue);
                            }
                        }
                        else
                        {
                            var columnValue = GetColumn(cell);
                            var rowIndex = GetRowIndex(cell);
                            var cellValue = await GetValue(int.Parse(cell.CellValue.InnerText));
                            if (int.Parse(rowIndex) >= 4)
                            {
                                dic.Add(columnValue, cellValue);
                            }
                        }
                    }
                    if (dic.Any())
                    {
                        items.Add(i++, dic);
                    }
                }
            }
            await Task.CompletedTask;
            return columns;
        }

        public static async Task<Dictionary<int, Dictionary<string, string>>> GetItems(FileStream stream)
        {
            document = SpreadsheetDocument.Open(stream, false);
            await Pars();
            return items;
        }

        private static async Task Pars()
        {
            var mysheet = (Sheet)document.WorkbookPart.Workbook.Sheets.ChildElements.GetItem(0);
            var wbPart = document.WorkbookPart;
            var worksheet = ((WorksheetPart)wbPart.GetPartById(mysheet.Id)).Worksheet;
            var sheetDatas = worksheet.Elements<SheetData>();
            int i = 0;
            foreach (var sheetData in sheetDatas)
            {
                var rows = sheetData.Elements<Row>();
                foreach (var row in rows)
                {
                    var dic = new Dictionary<string, string>();
                    foreach (var cell in row.Elements<Cell>())
                    {
                        if (cell.DataType == null)
                        {
                            if (cell.InnerText != null)
                            {
                                var columnValue = GetColumn(cell);
                                var rowIndex = GetRowIndex(cell);
                                var cellValue = cell.InnerText;
                                if (int.Parse(rowIndex) >= 4)
                                {
                                    dic.Add(columnValue, cellValue);
                                }
                            }
                        }
                        else
                        if (cell.DataType == CellValues.SharedString)
                        {
                            var columnValue = GetColumn(cell);
                            var rowIndex = GetRowIndex(cell);
                            var cellValue = await GetValue(int.Parse(cell.CellValue.InnerText));
                            if (int.Parse(rowIndex) >= 4)
                            {
                                dic.Add(columnValue, cellValue);
                            }
                        }
                        else
                        {
                            var columnValue = GetColumn(cell);
                            var rowIndex = GetRowIndex(cell);
                            var cellValue = await GetValue(int.Parse(cell.CellValue.InnerText));
                            if (int.Parse(rowIndex) >= 4)
                            {
                                dic.Add(columnValue, cellValue);
                            }
                        }
                    }
                    if (dic.Any())
                    {
                        items.Add(i++, dic);
                    }
                }
            }
        }

        private static async Task<string> GetValue(int index)
        {
            if (document == null)
            {
                return null;
            }
            if (!document.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Any())
            {
                return null;
            }
            var sharedStringTablePart = document.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
            var sharedStringTable = sharedStringTablePart.SharedStringTable;
            var result = sharedStringTable.ChildElements.GetItem(index);
            return await Task.FromResult(result.InnerText);
        }

        private static string GetRowIndex(Cell cell)
        {
            Regex regex = new Regex("[0-9]+");
            Match match = regex.Match(cell.CellReference);
            return match.Value;
        }

        private static string GetColumn(Cell cell)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cell.CellReference);
            return match.Value;
        }
    }
}