﻿namespace MD_S5.Identity
{
    public class LoginModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}