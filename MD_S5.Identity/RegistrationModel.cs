﻿namespace MD_S5.Identity
{
    public class RegistrationModel
    {
        public string UserName { get; set; }
        public string UserPhone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public long? CompanyId { get; set; }
        public int? RoleId { get; set; }
    }
}