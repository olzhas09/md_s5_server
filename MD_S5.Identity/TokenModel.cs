﻿using System;

namespace MD_S5.Identity
{
    public class TokenModel
    {
        public string Token { get; set; }
        public string RoleUser { get; set; }
        public long? UserId { get; set; }
        public DateTime ExpireDate { get; set; }
        public User User { get; set; }
    }
}