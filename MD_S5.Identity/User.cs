﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MD_S5.Identity
{
    public class User
    {
        public long Id { get; set; }
        public long? CompanyId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }

    public class AppUser : IdentityUser<long>
    {
        public long? CompanyId { get; set; }

        [NotMapped]
        public string Password { get; set; }

        [NotMapped]
        public string ConfirmPassword { get; set; }

        public static bool FindByAllColumns(IdentityUser<long> obj, string value, int depth = 0)
        {
            depth++;
            var result = false;
            var properties = obj?.GetType()?.GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                var propety = properties[i];
                var propertyValue = propety.GetValue(obj, null);
                if (propety.PropertyType.Namespace.Contains("MD_S5") && propertyValue is IdentityUser<long> && !propety.DeclaringType.IsEnum && propertyValue != null && depth < 2)
                {
                    result = FindByAllColumns((IdentityUser<long>)propertyValue, value, depth);
                }
                else
                if (propertyValue != null && propertyValue.ToString().ToLower().Contains(value.ToLower()))
                {
                    result = true;
                }
                if (result)
                {
                    return result;
                }
            }
            return result;
        }
    }

    public class AppRole : IdentityRole<long>
    {
    }

    public class AppDbContext : IdentityDbContext<AppUser, AppRole, long>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public AppDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<AppRole>(opt =>
            {
                opt.HasData(new List<AppRole>
                {
                    new AppRole{ ConcurrencyStamp=DateTime.UtcNow.ToString(), Id=1, Name="User", NormalizedName="USER"},
                    new AppRole{ ConcurrencyStamp=DateTime.UtcNow.ToString(), Id=2,Name="Administrator", NormalizedName="ADMINISTRATOR"},
                    new AppRole{ ConcurrencyStamp=DateTime.UtcNow.ToString(), Id=3,Name="Moderator", NormalizedName="MODERATOR"},
                });
            });
        }
    }
}