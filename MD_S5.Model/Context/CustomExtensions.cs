﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Linq;

namespace Microsoft.EntityFrameworkCore;

public static class CustomExtensions
{
    public static IQueryable Query(this DbContext context, string entityName)
    {
        return context.Query(context.Model.FindEntityType(entityName).ClrType);
    }

    public static IQueryable Query(this DbContext context, Type entityType)
    {
        return (IQueryable)((IDbSetCache)context).GetOrAddSet(context.GetDependencies().SetSource, entityType);
    }
}