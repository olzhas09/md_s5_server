﻿using MD_S5.Common.Model;
using MD_S5.Excell;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Threading.Tasks;

namespace MD_S5.Model
{
    public partial class Repository<TEntity, TKey>
    {
        public virtual async Task<MatchColumn> ImportExcelStepThree(string guid)
        {
            var importCache = new ImportCache(guid);
            object obj = (TEntity)Activator.CreateInstance(typeof(TEntity));
            var typeName = obj.GetType().Name;
            var main = importCache.GetListValue(typeName).OfType<IEntity<long>>();

            var find = main.Where(x => x.ImportStatus == ImportStatus.Find);
            var create = main.Where(x => x.ImportStatus == ImportStatus.Create);

            var dic = importCache.GetGuid(guid);
            var owner = await GetCompany();
            foreach (var keyList in dic)
            {
                if (keyList.Key != typeName)
                {
                    var findRef = keyList.Value.OfType<IEntity<long>>().Where(x => x.ImportStatus == ImportStatus.Find);
                    var createRef = keyList.Value.OfType<IEntity<long>>().Where(x => x.ImportStatus == ImportStatus.Create);
                    foreach (var refItem in findRef)
                    {
                        refItem.OwnerId = owner?.Id;
                        _context.Update(refItem);
                        await _context.SaveChangesAsync();
                    }
                    foreach (var refItem in createRef)
                    {
                        refItem.OwnerId = owner?.Id;
                        await _context.AddAsync(refItem);
                        await _context.SaveChangesAsync();
                    }
                }
            }

            await UpdateRange(find);
            await CreateRange(create);

            return new MatchColumn { CreateRows = create.Count(), UpdateRows = find.Count() };
        }

        public async Task<UploadFile> ImportExcelStepOne(Stream fileStream)
        {
            var colums = await ImportExcel.GetHeader(fileStream);
            return colums;
        }

        public async Task<MatchColumn> ImportExcelStepTwo(Stream fileStream, List<Column> columns, List<Column> columnsExport)
        {
            var guid = Guid.NewGuid().ToString();
            var importCache = new ImportCache(guid);
            var dic = new Dictionary<int, Dictionary<string, (Column, object)>>();
            var dt = ImportExcel.ReadExcelSheet(fileStream, true);
            CreateDic(columns, dic, dt);
            var createList = new List<object>();
            var updateList = new List<object>();
            foreach (var item in dic)
            {
                var uniqueList = columns.Where(x => x.UniqueField).ToList();

                object obj = (TEntity)Activator.CreateInstance(typeof(TEntity));
                string typeName = obj.GetType().Name;
                var findObj = await FindUnique(obj, uniqueList, item.Value);

                if (findObj != null)
                {
                    obj = findObj;
                    obj.GetType().GetProperty(nameof(ImportStatus)).SetValue(obj, ImportStatus.Find);
                }
                else
                {
                    obj.GetType().GetProperty(nameof(ImportStatus)).SetValue(obj, ImportStatus.Create);
                }

                importCache.SetValue(obj, typeName);

                foreach (var val in item.Value)
                {
                    var property = obj.GetType().GetProperty(val.Key);
                    var column = val.Value.Item1;
                    if (property != null)
                    {
                        object refItem = null;
                        try
                        {
                            if (val.Value.Item2 is DBNull)
                            {
                                continue;
                            }
                            if (val.Value.Item1.SubColumn.SubField != null)
                            {
                                refItem = await GetOrAddReference(obj, val, property, column.CreateReference, importCache);
                            }
                            if (refItem == null && val.Value.Item1.SubColumn.SubField == null)
                            {
                                obj.GetType().GetProperty(val.Key).SetValue(obj, val.Value.Item2);
                            }
                            else
                            {
                                obj.GetType().GetProperty(val.Key).SetValue(obj, refItem);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                if (findObj != null)
                {
                    updateList.Add(obj);
                }
                else
                {
                    createList.Add(obj);
                }
            }
            var list = updateList.ToList();
            list.AddRange(createList);
            return (new MatchColumn { CreateRows = createList.Count, UpdateRows = updateList.Count, Entities = list, Guid = guid });
        }

        private void CreateDic(List<Column> columns, Dictionary<int, Dictionary<string, (Column, object)>> dic, (DataTable, List<Column>) dt)
        {
            for (int i = 0; i < dt.Item1.Rows.Count; i++)
            {
                var dicRow = new Dictionary<string, (Column, object)>();
                foreach (var column in columns)
                {
                    var item = dt.Item1.Rows[i][column.Header];
                    if (column.SubColumn == null)
                    {
                        continue;
                    }
                    dicRow.Add(column.SubColumn?.Field?.FirstCharToUpper(), (column, item));
                }
                dic.Add(i, dicRow);
            }
        }

        private async Task<object> FindUnique(object obj, List<Column> columns, Dictionary<string, (Column, object)> value)
        {
            if (columns.Count == 0)
            {
                return null;
            }
            var query = "";
            int i = 0;
            foreach (var column in columns)
            {
                i++;
                var field = column.SubColumn.Field.FirstCharToUpper();
                if (columns.Count == i)
                {
                    query += $"{ field} == \"{value[field].Item2}\"";
                }
                else
                {
                    query += $"{ field} == \"{value[field].Item2}\" && ";
                }
            }

            var findsValue = await _context.Query(obj.GetType()).Where(query).ToDynamicListAsync();
            return findsValue.Count > 0 ? findsValue[0] : null;
        }

        private async Task<object> GetOrAddReference(object mainObject, KeyValuePair<string, (Column, object)> val, PropertyInfo property, bool create = false, ImportCache importCache = null)
        {
            var value = val.Value.Item1.SubColumn.SubField.FirstCharToUpper();
            var referenceType = property.PropertyType;

            var query = $"{ value} == \"{val.Value.Item2}\"";
            var localDbSet = _context.Query(referenceType);
            object findValue = _context.Query(referenceType).FirstOrDefault(query);
            if (findValue != null)
            {
                findValue.GetType().GetProperty(nameof(ImportStatus)).SetValue(findValue, ImportStatus.Find);
            }
            if (create && findValue == null)
            {
                var obj = Activator.CreateInstance(referenceType);
                obj.GetType().GetProperty(value).SetValue(obj, val.Value.Item2);
                var owner = await GetCompany();
                obj.GetType().GetProperty("OwnerId").SetValue(obj, owner?.Id);
                _context.Add(obj);
                await _context.SaveChangesAsync();
                findValue = obj;
                findValue.GetType().GetProperty(nameof(ImportStatus)).SetValue(findValue, ImportStatus.Create);
            }
            importCache.SetValue(findValue, value, val.Value.Item2);
            return findValue;
        }
    }
}