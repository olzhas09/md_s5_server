﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace MD_S5.Model
{
    public class ImportCache
    {
        public static ConcurrentDictionary<string, ConcurrentDictionary<string, List<object>>> Cache = new ConcurrentDictionary<string, ConcurrentDictionary<string, List<object>>>();

        public ImportCache(string guid)
        {
            Guid = guid;
        }

        public string Guid { get; set; }

        public object SetValue(object obj, string value, object item2)
        {
            if (obj == null)
                return null;
            var typeName = obj.GetType().Name;
            Cache.TryGetValue(Guid, out var pairs);
            if (pairs == null)
            {
                pairs = new ConcurrentDictionary<string, List<object>>();
                Cache.TryAdd(Guid, pairs);
            }
            pairs.TryGetValue(typeName, out var list);
            if (list == null)
            {
                list = new List<object>();
                pairs.TryAdd(typeName, list);
            }

            var query = $"{ value} == \"{item2}\"";

            object item = list.AsQueryable().ToList().FirstOrDefault(query);

            if (item == null)
            {
                list.Add(obj);
                item = obj;
            }

            return item;
        }

        public object SetValue(object obj, string typeName)
        {
            if (obj == null)
                return null;
            Cache.TryGetValue(Guid, out var pairs);
            if (pairs == null)
            {
                pairs = new ConcurrentDictionary<string, List<object>>();
                Cache.TryAdd(Guid, pairs);
            }
            pairs.TryGetValue(typeName, out var list);
            if (list == null)
            {
                list = new List<object>();
                pairs.TryAdd(typeName, list);
            }

            list.Add(obj);

            return obj;
        }

        public List<object> GetListValue(string typeName)
        {
            Cache.TryGetValue(Guid, out var pairs);
            pairs.TryGetValue(typeName, out var list);
            return list;
        }

        public ConcurrentDictionary<string, List<object>> GetGuid(string guid)
        {
            Cache.TryGetValue(guid, out var pairs);
            return pairs;
        }
    }
}