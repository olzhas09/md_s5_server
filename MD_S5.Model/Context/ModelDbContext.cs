﻿using MD_S5.Common.Model;
using MD_S5.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace MD_S5.Model.Context
{
    public class ModelDbContext : DbContext
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ModelDbContext(DbContextOptions<ModelDbContext> options, UserManager<AppUser> userManager = null, IHttpContextAccessor httpContextAccessor = null) : base(options)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        public ModelDbContext()
        {
        }

        public DbSet<Temp> Temps { get; set; }
        public DbSet<Project> Projects { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            var userId = _httpContextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            foreach (var entityEntry in ChangeTracker.Entries())
            {
                if (entityEntry.Entity is IEntity<long> savedItemLong)
                {
                    if (savedItemLong.Status == StatusModel.New)
                    {
                        savedItemLong.CreateDate = DateTime.UtcNow;
                        if (userId != null)
                        {
                            savedItemLong.CreateUserId = long.Parse(userId);
                        }
                    }
                    if (userId != null)
                    {
                        savedItemLong.UpdateUserId = long.Parse(userId);
                    }
                    savedItemLong.UpdateDate = DateTime.UtcNow;
                    savedItemLong.Status = StatusModel.Actual;
                }
                else if (entityEntry.Entity is IEntity<int> savedItemInt)
                {
                    if (savedItemInt.Status == StatusModel.New)
                    {
                        savedItemInt.CreateDate = DateTime.UtcNow;
                        if (userId != null)
                        {
                            savedItemInt.CreateUserId = long.Parse(userId);
                        }
                    }
                    if (userId != null)
                    {
                        savedItemInt.UpdateUserId = long.Parse(userId);
                    }
                    savedItemInt.UpdateDate = DateTime.UtcNow;
                    savedItemInt.Status = StatusModel.Actual;
                }
            }

            return await base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}