﻿using MD_S5.Common.Group;
using MD_S5.Common.Model;
using MD_S5.Common.Notify;
using MD_S5.Common.Repository;
using MD_S5.Excell;
using MD_S5.Identity;
using MD_S5.Model.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MD_S5.Model
{
    public partial class Repository<TEntity, TKey> : IRepository<TEntity, TKey>
    where TEntity : class, IEntity<TKey>
    where TKey : struct
    {
        protected readonly DbContext _context;
        protected IHttpContextAccessor _httpContextAccessor;
        protected JsonSerializerSettings serializerSettings;
        protected IDistributedCache _distributedCache;
        public Project Owner;
        public List<string> Roles;

        private readonly UserManager<AppUser> _userManager;

        public Repository(ModelDbContext context, UserManager<AppUser> userManager = null, IHttpContextAccessor httpContextAccessor = null, IDistributedCache distributedCache = null)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _context = context;
            _distributedCache = distributedCache;
            serializerSettings = new JsonSerializerSettings();
            serializerSettings.Converters.Add(new StringEnumConverter());
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializerSettings.NullValueHandling = NullValueHandling.Ignore;
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        }

        private async Task<Project> GetCompany()
        {
            var userId = _httpContextAccessor?.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (Owner != null)
            {
                return Owner;
            }
            if (_userManager == null)
            {
                return null;
            }
            var user = await _userManager?.GetUserAsync(_httpContextAccessor?.HttpContext?.User);
            if (user == null)
            {
                return null;
            }
            var company = await _context.Set<Project>().FirstOrDefaultAsync(x => x.Id == user.CompanyId);
            return company;
        }

        private async Task<List<string>> GetRoles()
        {
            if (Roles != null)
            {
                return Roles;
            }
            if (_userManager == null)
            {
                return new List<string>();
            }
            var user = await _userManager.GetUserAsync(_httpContextAccessor.HttpContext?.User);
            if (user == null)
            {
                return new List<string>();
            }
            var roles = await _userManager.GetRolesAsync(user);
            return roles.ToList();
        }

        public string UserId => _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);

        public string RepositoryName => typeof(TEntity).Name;

        public virtual async Task<TEntity> Create(TEntity entity)
        {
            try
            {
                var roles = await GetRoles();
                _context.Attach(entity);
                await _context.Set<TEntity>().AddAsync(entity);
                var owner = await GetCompany();
                entity.OwnerId = owner?.Id;
                entity.CreateDate = DateTime.UtcNow;
                entity.UpdateDate = DateTime.UtcNow;
                var item = await Save(entity);
                NotifyService.NotifyAll(UserId, item.GetType().Name, "Create", JsonConvert.SerializeObject(item, serializerSettings));
                await _context.Set<Temp>().AddAsync(new Temp { TypeName = item.GetType().Name, ValueId = item.Id.ToString(), SavedStatus = "Create", Value = JsonConvert.SerializeObject(item, serializerSettings) });
                await _context.SaveChangesAsync();
                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Delete(TKey id)
        {
            var owner = await GetCompany();
            var entity = await _context.Set<TEntity>().FindAsync(id);

            var roles = await GetRoles();
            if (!roles.Contains("User") || !roles.Contains("Administrator") && roles.Count > 0 && entity.OwnerId != owner?.Id) throw new Exception("Access Denied");
            _context.Remove(entity);
            var item = await Save(entity);
            NotifyService.NotifyAll(UserId, nameof(item), "Delete", JsonConvert.SerializeObject(item, serializerSettings));
            await _context.Set<Temp>().AddAsync(new Temp { TypeName = item.GetType().Name, ValueId = item.Id.ToString(), SavedStatus = "Delete", Value = JsonConvert.SerializeObject(item, serializerSettings) });
            await _context.SaveChangesAsync();
            return (TEntity)entity;
        }

        public virtual async Task<TEntity> Delete(TEntity entity)
        {
            _context.Remove(entity);
            await Save(entity);
            NotifyService.NotifyAll(UserId, entity.GetType().Name, "Delete", JsonConvert.SerializeObject(entity, serializerSettings));
            await _context.Set<Temp>().AddAsync(new Temp { TypeName = entity.GetType().Name, ValueId = entity.Id.ToString(), SavedStatus = "Delete", Value = JsonConvert.SerializeObject(entity, serializerSettings) });
            await _context.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<IEnumerable<TEntity>> Get()
        {
            var owner = await GetCompany();
            var roles = await GetRoles();
            if (owner != null && roles.Contains("User") || !roles.Contains("Administrator") && roles.Count > 0 && roles.Count > 0)
            {
                if (typeof(TEntity) == typeof(Project))
                    return await _context.Set<TEntity>().ToListAsync();
                else
                    return await _context.Set<TEntity>().Where(x => x.OwnerId == owner.Id).OrderBy(x => x.Id).ToListAsync();
            }
            else
            {
                return await _context.Set<TEntity>().OrderBy(x => x.Id).ToListAsync();
            }
        }

        public virtual async Task<TEntity> GetById(TKey id)
        {
            var owner = await GetCompany();
            var entity = await _context.Set<TEntity>().FindAsync(id);
            var roles = await GetRoles();
            if (!roles.Contains("User") || !roles.Contains("Administrator") && roles.Count > 0 && entity.OwnerId != owner.Id) throw new Exception("Access Denied");

            return (TEntity)entity;
        }

        public virtual async Task<TEntity> Update(TEntity entity)
        {
            try
            {
                var properties = entity.GetType().GetProperties();
                foreach (var property in properties)
                {
                    if (property.Name.Length < 2)
                        continue;
                    if (property.Name.Substring(property.Name.Length - 2, 2).Contains("Id"))
                        continue;
                    var propertyId = properties.FirstOrDefault(x => x.Name == $"{property.Name}Id")?.Name;
                    var value = entity.GetType().GetProperty(property.Name).GetValue(entity);
                    if (propertyId == null || value == null)
                        continue;
                    var id = value.GetType().GetProperty("Id").GetValue(value);
                    entity.GetType().GetProperty(propertyId).SetValue(entity, id);
                }
                entity.UpdateDate = DateTime.UtcNow;
                //var search = _context.Set<TEntity>().Find(entity.Id);
                //var changeProperty = await GetChangeProperty(entity, search);
                //_context.Set<TEntity>().Local.Remove(search);
                //_context.Set<Company>().Local.Remove((Company)search.GetType().GetProperty("Company").GetValue(search, null));
                _context.Entry(entity).State = EntityState.Modified;
                var owner = await GetCompany();
                var item = await Save((TEntity)entity);
                var roles = await GetRoles();
                if (!roles.Contains("User") || !roles.Contains("Administrator") && roles.Count > 0 && item.OwnerId != owner.Id) throw new Exception("Access Denide");
                NotifyService.NotifyAll(UserId, item.GetType().Name, "Update", JsonConvert.SerializeObject(item, serializerSettings));
                await _context.Set<Temp>().AddAsync(new Temp { TypeName = item.GetType().Name, ValueId = item.Id.ToString(), SavedStatus = "Update", Value = JsonConvert.SerializeObject(item, serializerSettings) });
                await _context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<IEnumerable<string>> GetChangeProperty(TEntity entity, TEntity search)
        {
            var changeProperty = new List<string>();
            var errorList = new List<string>();
            foreach (var property in entity.GetType().GetProperties())
            {
                if (property.Name == "Id" || property.Name == "Editing" || property.Name == "UpdateDate")
                    continue;

                var val1 = entity.GetType().GetProperty(property.Name).GetValue(entity, null);
                var val2 = search.GetType().GetProperty(property.Name).GetValue(search, null);
                var prop = property.Name.Substring(0, property.Name.Length - 2);

                if (property.Name.Substring(property.Name.Length - 2, 2).Contains("Id"))
                {
                    if (entity.GetType().GetProperties().FirstOrDefault(x => x.Name == prop)?.Name != null)
                        errorList.Add(entity.GetType().GetProperties().FirstOrDefault(x => x.Name == prop)?.Name);
                }

                if (errorList.Contains(property.Name))
                    continue;

                if (val1 == null && val2 == null)
                    continue;

                if (!val1.Equals(val2))
                    changeProperty.Add(property.Name);
            }
            await Task.CompletedTask;
            return changeProperty;
        }

        public virtual async Task<IEnumerable<Temp>> History(TKey id)
        {
            var items = (List<Temp>)null;
            if (id is long l && l == 0)
            {
                items = await _context.Set<Temp>().Where(x => x.TypeName == typeof(TEntity).BaseType.Name).ToListAsync();
            }
            else
            {
                items = await _context.Set<Temp>().Where(x => x.TypeName == typeof(TEntity).Name && x.ValueId == id.ToString()).ToListAsync();
            }
            var list = items.Select(x => JsonConvert.DeserializeObject<TEntity>(x.Value)).ToList();
            foreach (var item in items)
            {
                var user = await _userManager.FindByIdAsync(item.UpdateUserId.ToString());
                item.User = user;
            }
            return items;
        }

        public IEnumerable<TEntity> Where(Func<TEntity, bool> func)
        {
            var items = _context.Set<TEntity>().Where(func).ToList();
            return items;
        }

        public virtual async Task<TEntity> Save(TEntity entity)
        {
            entity.Status = StatusModel.Actual;
            await _context.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<PaginatorContainer<TEntity>> GetOrFind(string order, int skip, int take, Column[] colums, string value = "", string property = "", int desc = 1)
        {
            var query = "";
            for (int i = 0; i < colums.Length; i++)
            {
                var item = colums[i];
                var b = typeof(TEntity).GetProperty(item.Field.FirstCharToUpper())?.PropertyType;
                if (item.Type == "text" && b == typeof(string) && !string.IsNullOrEmpty(value))
                {
                    query += $" {ParseColumnText(item)}.ToLower().Contains(\"" + value.ToLower() + "\")";
                    if (i != colums.Length - 1)
                    {
                        query += " or";
                    }
                }
            }

            var entityType = _context.Model.FindEntityType(typeof(TEntity));
            var schema = entityType.GetSchema();
            var tableName = entityType.GetTableName();
            var parseQuery = "";
            var segments = order.Split('.');
            int index = 0;
            foreach (var segment in segments)
            {
                parseQuery += segment.FirstCharToUpper();
                index++;
                if (segments.Count() > 1 && index == 1)
                {
                    parseQuery += ".";
                }
            }
            var items = (List<TEntity>)null;
            var descStr = desc == 1 ? "" : "desc";
            var count = 0;
            if (!string.IsNullOrEmpty(value))
            {
                var config = new ParsingConfig
                {
                    IsCaseSensitive = false
                };
                query = query.Remove(query.Length - 1);
                query = query.Remove(query.Length - 1);

                items = _context.Set<TEntity>().AsQueryable().Where(config, query).OrderBy($"np({parseQuery})" + descStr).ToList();
                count = items.Count;
                items = items.Skip(skip).Take(take).ToList();
                return new PaginatorContainer<TEntity> { Items = items, CountTotal = count };
            }

            items = _context.Set<TEntity>().AsQueryable().OrderBy($"np({parseQuery})" + descStr).ToList();
            count = items.Count;
            await Task.CompletedTask;
            items = items.Skip(skip).Take(take).ToList();
            return new PaginatorContainer<TEntity> { CountTotal = count, Items = items };
        }

        private string ParseColumnText(Column item)
        {
            if (!string.IsNullOrEmpty(item.SubField))
            {
                return $"{item.Field.FirstCharToUpper()}.{item.SubField.FirstCharToUpper()}";
            }
            return item.Field.FirstCharToUpper();
        }

        public async Task<IEnumerable<WbsByField>> GetWbsByFields(long? id)
        {
            try
            {
                var companyProperty = typeof(TEntity).GetProperties().FirstOrDefault(x => x.Name == "Company");
                var properties = typeof(TEntity).GetProperties().Where(x => x.PropertyType.Namespace == "MD_S5.Model").ToList();
                properties.Add((System.Reflection.PropertyInfo)companyProperty);
                var entityList = await _context.Set<TEntity>().ToListAsync();
                var wbsList = new List<WbsByField>();
                foreach (var item in properties)
                {
                    if (item.PropertyType.BaseType.Name == "Enum" || item.Name == "Parent")
                        continue;
                    var parent = GetParentField(item.Name);
                    var list = CreateWBSField(parent, item, entityList, id);
                    if (list.Count() == 0)
                        continue;
                    wbsList.AddRange(list);
                }
                return wbsList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IEnumerable<WbsByField> CreateWBSField(WbsByField parent, System.Reflection.PropertyInfo property, List<TEntity> entityList, long? id)
        {
            var listWbs = new List<WbsByField>();
            foreach (var item in entityList)
            {
                var wbs = new WbsByField();
                wbs.Guid = Guid.NewGuid();
                wbs.TypeId = long.Parse(item.Id.ToString());
                wbs.Type = item;
                wbs.TypeName = item.GetType().Name;
                wbs.Parent = parent;
                wbs.Reference = item.GetType().GetProperty(property.Name).GetValue(item, null);
                wbs.ReferenceName = property.Name;
                if (wbs.Reference != null)
                {
                    wbs.ReferenceId = long.Parse(wbs.Reference.GetType().GetProperty("Id").GetValue(wbs.Reference, null).ToString());
                    wbs.Childs = new List<object>(entityList.Where(x => x.GetType()?.GetProperty($"{wbs.ReferenceName}")?.GetValue(x, null) == wbs.Reference).ToList());
                }
                if (wbs.Reference != null)
                    listWbs.Add(wbs);
            }
            var group = listWbs.GroupBy(x => x.ReferenceId).Select(p => new WbsByField()
            {
                WbsType = WbsType.Group,
                ReferenceId = p.Key,
                Reference = p.FirstOrDefault(x => x.ReferenceId == p.Key).Reference,
                ReferenceName = p.FirstOrDefault(x => x.ReferenceId == p.Key).ReferenceName,
                Parent = p.FirstOrDefault(x => x.ReferenceId == p.Key).Parent,
                Childs = p.FirstOrDefault(x => x.ReferenceId == p.Key).Childs,
                Guid = p.FirstOrDefault(x => x.ReferenceId == p.Key).Guid,
                ParentId = p.FirstOrDefault(x => x.ReferenceId == p.Key).ParentId,
                TypeName = p.FirstOrDefault(x => x.ReferenceId == p.Key).TypeName,
            });
            return group;
        }

        public WbsByField GetParentField(string param)
        {
            return new WbsByField
            {
                Guid = Guid.NewGuid(),
                TypeName = typeof(TEntity).Name,
                ReferenceName = param,
                WbsType = WbsType.Parent
            };
        }

        public async Task<FileStreamResult> ExportExcel(Column[] cols)
        {
            var list = await Get();
            var file = await Excell.ExportExcel.Create((System.Collections.IList)list, "", cols);
            var ext = Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse(file.Item2);
            return new FileStreamResult(file.Item1, ext);
        }

        public virtual void Verification(long? id, ref List<TEntity> entityList)
        {
        }

        public virtual async Task<IEnumerable<TEntity>> GetUsingRedisCache()
        {
            var cacheKey = typeof(TEntity).Name;
            string serializedCustomerList;
            var customerList = new List<TEntity>();
            var redisCustomerList = await _distributedCache.GetAsync(cacheKey);
            if (redisCustomerList != null)
            {
                serializedCustomerList = Encoding.UTF8.GetString(redisCustomerList);
                customerList = JsonConvert.DeserializeObject<List<TEntity>>(serializedCustomerList);
            }
            else
            {
                customerList = await _context.Set<TEntity>().ToListAsync();
                serializedCustomerList = JsonConvert.SerializeObject(customerList);
                redisCustomerList = Encoding.UTF8.GetBytes(serializedCustomerList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTime.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await _distributedCache.SetAsync(cacheKey, redisCustomerList, options);
            }
            return customerList;
        }

        public virtual async Task<IEnumerable<object>> CreateRange(IEnumerable<object> range)
        {
            var owner = await GetCompany();
            foreach (TEntity item in range)
            {
                item.OwnerId = owner?.Id;
            }
            await _context.AddRangeAsync(range);
            await _context.SaveChangesAsync();
            return range;
        }

        public virtual async Task<IEnumerable<object>> UpdateRange(IEnumerable<object> range)
        {
            var owner = await GetCompany();
            foreach (TEntity item in range)
            {
                item.OwnerId = owner?.Id;
            }
            _context.UpdateRange(range);
            await _context.SaveChangesAsync();
            return range;
        }
    }
}