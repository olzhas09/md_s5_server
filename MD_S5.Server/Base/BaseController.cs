﻿using MD_S5.Common.Group;
using MD_S5.Common.Model;
using MD_S5.Common.Repository;
using MD_S5.Excell;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MD_S5.Server.Base
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public abstract class BaseController<TEntity, TKey, TService> : ControllerBase
        where TEntity : IEntity<TKey>
        where TKey : struct
        where TService : IRepository<TEntity, TKey>
    {
        protected TService _service;
        private IWebHostEnvironment _hostingEnvironment;

        protected BaseController(TService service, IWebHostEnvironment hostingEnvironment)
        {
            _service = service;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet("GetList")]
        public virtual async Task<ActionResult<IEnumerable<TEntity>>> Get()
        {
            try
            {
                var list = await _service.Get();
                return Ok(list);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetById/{id}")]
        public async Task<ActionResult<TEntity>> GetById(TKey id)
        {
            try
            {
                return Ok(await _service.GetById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("Post")]
        public virtual async Task<ActionResult<TEntity>> Post(TEntity item)
        {
            try
            {
                return Ok(await _service.Create(item));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("Put")]
        public async Task<ActionResult<TEntity>> Put(TEntity item)
        {
            try
            {
                return Ok(await _service.Update(item));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("Delete")]
        public virtual async Task<TEntity> Delete(TEntity item)
        {
            return await _service.Delete(item);
        }

        [HttpPost("GetOrFind")]
        public async Task<ActionResult<PaginatorContainer<TEntity>>> GetOrFind([FromQuery] string order,
            [FromQuery] int skip,
            [FromQuery] int take,
            [FromQuery] string value,
            [FromQuery] string property,
            [FromQuery] int desc,
            [FromBody] Column[] colums)
        {
            try
            {
                var items = await _service.GetOrFind(order: order,
                   skip: skip,
                   take: take,
                   property: property,
                   value: value,
                   desc: desc,
                   colums: colums);
                return Ok(items);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("HistoryById/{id}")]
        public async Task<ActionResult<IEnumerable<Temp>>> History(TKey id)
        {
            try
            {
                var items = await _service.History(id);
                return Ok(items);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetWbsByFields")]
        public async Task<ActionResult<IEnumerable<WbsByField>>> GetWbsByFields()
        {
            try
            {
                var id = 0;
                return Ok(await _service.GetWbsByFields(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("ExportExcel")]
        public async Task<ActionResult<FileStreamResult>> ExportExcel([FromBody] Column[] colums)
        {
            try
            {
                return Ok(await _service.ExportExcel(colums));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("ImportExcelStepOne"), DisableRequestSizeLimit]
        public async Task<ActionResult<UploadFile>> ImportExcelStepOne()
        {
            try
            {
                var file = Request.Form.Files[0];
                var item = await _service.ImportExcelStepOne(file.OpenReadStream());
                item.NameFile = file.FileName;
                item.SizeFile = file.Length;
                return Ok(item);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("ImportExcelStepTwo"), DisableRequestSizeLimit]
        public async Task<ActionResult<MatchColumn>> ImportExcelStepTwo()
        {
            try
            {
                var nameService = _service.RepositoryName;
                var file = Request.Form.Files[0];
                var columnsStr = Request.Form["text"];
                var columnsStr1 = Request.Form["text1"];
                var columns = JsonConvert.DeserializeObject<Column[]>(columnsStr);
                var columns1 = JsonConvert.DeserializeObject<Column[]>(columnsStr1);
                var item = await _service.ImportExcelStepTwo(file.OpenReadStream(), columns.ToList(), columns1.ToList());
                HttpContext.Response.Headers.Add("X-Total-Create", item.CreateRows.ToString());
                HttpContext.Response.Headers.Add("X-Total-Update", item.UpdateRows.ToString());
                return Ok(item);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("ImportExcelStepThree")]
        public async Task<ActionResult<MatchColumn>> ImportExcelStepThree(string guid)
        {
            try
            {
                return Ok(await _service.ImportExcelStepThree(guid));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetRedis")]
        public async Task<IEnumerable<TEntity>> GetRedis()
        {
            return await _service.GetUsingRedisCache();
        }
    }
}