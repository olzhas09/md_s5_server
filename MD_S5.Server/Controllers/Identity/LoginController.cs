﻿using MD_S5.Identity;
using MD_S5.Model.Context;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MD_S5.Server.Controllers.Identity
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly IStringLocalizer<LoginController> _localizer;
        private readonly IConfiguration _config;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ModelDbContext _db;

        public LoginController(IConfiguration config, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, ModelDbContext db, IStringLocalizer<LoginController> localizer)
        {
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;
            _db = db;
            _localizer = localizer;
        }

        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json", Type = typeof(TokenModel))]
        public async Task<IActionResult> SignIn([FromBody] LoginModel login)
        {
            if (login == null)
            {
                return BadRequest("Null");
            }
            if (string.IsNullOrEmpty(login.Login) && string.IsNullOrEmpty(login.Password))
            {
                return BadRequest(_localizer["LoginOrPasswordError"]);
            }
            var response = (TokenModel)null;//Unauthorized();
            var user = await AuthenticateUser(login).ConfigureAwait(false);
            if (user == null)
            {
                var error = _localizer["LoginOrPasswordError"];
                return BadRequest(error);
            }
            if (user != null)
            {
                var roles = await _userManager.GetRolesAsync(user);

                var token = await GenerateJSONWebToken(user).ConfigureAwait(false);
                response = new TokenModel()
                {
                    Token = token.Item1,
                    UserId = user.Id,
                    ExpireDate = token.Item2,
                    User = new User
                    {
                        CompanyId = user.CompanyId,
                        Email = user.Email,
                        Id = user.Id,
                        UserName = user.UserName
                    }
                };
                return Ok(response);
            }
            return null;
        }

        private async Task<(string, DateTime)> GenerateJSONWebToken(AppUser user)
        {
            await ClearClaims(user).ConfigureAwait(false);
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new List<Claim> {
                new Claim(JwtRegisteredClaimNames.Sub,
                          value: user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sid, user.Id.ToString()),
            };

            foreach (var roleClaim in await _userManager.GetRolesAsync(user).ConfigureAwait(false))
            {
                claims.Add(new Claim(ClaimTypes.Role, roleClaim));
            }
            await _userManager.AddClaimsAsync(user, claims).ConfigureAwait(false);

            var expireDate = DateTime.UtcNow.AddDays(30);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims,
                expires: expireDate,
                signingCredentials: credentials);
            return (new JwtSecurityTokenHandler().WriteToken(token), expireDate);
        }

        private async Task<AppUser> AuthenticateUser(LoginModel login)
        {
            if (login == null)
            {
                return null;
            }

            var findUser = new AppUser();

            if (login.Login != null)
            {
                findUser = await _userManager.FindByNameAsync(login.Login).ConfigureAwait(false);
            }
            if (findUser == null && login.Login != null)
            {
                findUser = await _userManager.FindByEmailAsync(login.Login).ConfigureAwait(false);
            }
            if (findUser == null && login.Login != null)
            {
                findUser = await _userManager.Users.FirstOrDefaultAsync(s => s.PhoneNumber == login.Login).ConfigureAwait(false);
            }
            var result = await _userManager.CheckPasswordAsync(findUser, login.Password).ConfigureAwait(false);
            if (result)
            {
                await _signInManager.SignInAsync(findUser, true).ConfigureAwait(false);
                return findUser;
            }
            return null;
        }

        [HttpGet("SignOut")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Signout()
        {
            var identity = HttpContext.User;
            if (identity == null)
            {
                return BadRequest();
            }
            await _signInManager.SignOutAsync().ConfigureAwait(false);
            return Ok("SignOut");
        }

        [HttpGet("IsSigned")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public bool IsSigned()
        {
            bool isAuthenticated = User.Identity.IsAuthenticated;
            return isAuthenticated;
        }

        private async Task ClearClaims(AppUser user)
        {
            try
            {
                var claimsUser = await _userManager.GetClaimsAsync(user).ConfigureAwait(false);
                await _userManager.RemoveClaimsAsync(user, claimsUser).ConfigureAwait(false);
            }
            catch { }
        }
    }
}