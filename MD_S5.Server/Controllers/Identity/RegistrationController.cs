﻿using MD_S5.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MD_S5.Server.Controllers.Identity
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;

        public RegistrationController(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RegistrationModel value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            var user = new AppUser
            {
                PhoneNumber = value.UserPhone,
                Email = value.Email,
                UserName = value.UserName
            };
            var resultUser = await _userManager.CreateAsync(user, value.Password);
            if (resultUser.Succeeded)
            {
                var resultRole = await _userManager.AddToRoleAsync(user, "USER");
                if (resultRole.Succeeded)
                {
                }
            }
            else
            {
                return Problem(resultUser.ToString());
            }
            return Ok();
        }
    }
}