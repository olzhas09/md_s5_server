﻿using MD_S5.Common.Model;
using MD_S5.Common.Repository;
using MD_S5.Model;
using Microsoft.Extensions.DependencyInjection;

namespace MD_S5.Server.DI
{
    public static class RepositoryDI
    {
        public static void AddRepositoryDI(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Project, long>, Repository<Project, long>>();
            services.AddScoped<IRepository<Temp, long>, Repository<Temp, long>>();
        }
    }
}