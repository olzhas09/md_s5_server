﻿using MD_S5.Common.Model;
using MD_S5.Identity;
using MD_S5.Model.Context;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace MD_S5.Server.Hubs
{
    public class ConnectionUser
    {
        public string ConnectionId { get; set; }
        public long? UserId { get; set; }
        public long? CompanyId { get; set; }
    }

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class NotificationHub : Hub
    {
        public static ConcurrentDictionary<string, ConnectionUser> ConnectedIds = new ConcurrentDictionary<string, ConnectionUser>();

        private readonly UserManager<AppUser> _userManager;
        private readonly ModelDbContext _db;

        public NotificationHub(UserManager<AppUser> userManager, ModelDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        public override async Task OnConnectedAsync()
        {
            ConnectedIds.TryAdd(Context.ConnectionId, await GetUser(Context.User));
            await base.OnConnectedAsync();
        }

        private async Task<ConnectionUser> GetUser(System.Security.Claims.ClaimsPrincipal userPrinciple)
        {
            var user = await _userManager.GetUserAsync(userPrinciple);
            var company = await _db.Set<Project>().FindAsync(user.CompanyId);
            return new ConnectionUser
            {
                UserId = user.Id,
                CompanyId = company?.Id
            };
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            ConnectedIds.TryRemove(Context.ConnectionId, out var value);
            return base.OnDisconnectedAsync(exception);
        }
    }
}