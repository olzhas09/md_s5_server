﻿using System.Threading.Tasks;

namespace MD_S5.Server
{
    public interface IInit
    {
        Task Init();
    }
}