﻿using MD_S5.Identity;
using MD_S5.Model.Context;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace MD_S5.Server
{
    public class IdentityInit : IInit
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly ModelDbContext _dbContext;

        public IdentityInit(UserManager<AppUser> userManager, ModelDbContext dbContext, RoleManager<AppRole> roleManager)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _roleManager = roleManager;
        }

        public async Task Init()
        {
            await GenerateUser();
        }

        private async Task GenerateUser()
        {
            var user = new AppUser
            {
                PhoneNumber = "+7",
                Email = "admin@admin.com",
                UserName = "Administrator"
            };

            var user1 = new AppUser
            {
                PhoneNumber = "+78",
                Email = "admin1@admin.com",
                UserName = "Administrator1"
            };

            var resultUser = await _userManager.CreateAsync(user, "Aa123456").ConfigureAwait(false);
            if (resultUser.Succeeded)
            {
                var resultRole1 = await _userManager.AddToRoleAsync(user, "USER").ConfigureAwait(false);
                var resultRole2 = await _userManager.AddToRoleAsync(user, "ADMINISTRATOR").ConfigureAwait(false);
                if (resultRole1.Succeeded && resultRole2.Succeeded)
                {
                    await _dbContext.SaveChangesAsync();
                }
            }

            var resultUser1 = await _userManager.CreateAsync(user1, "Aa123456").ConfigureAwait(false);
            if (resultUser1.Succeeded)
            {
                var resultRole1 = await _userManager.AddToRoleAsync(user1, "USER").ConfigureAwait(false);
                var resultRole2 = await _userManager.AddToRoleAsync(user1, "ADMINISTRATOR").ConfigureAwait(false);
                if (resultRole1.Succeeded && resultRole2.Succeeded)
                {
                    await _dbContext.SaveChangesAsync();
                }
            }
        }
    }
}